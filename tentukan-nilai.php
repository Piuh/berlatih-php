<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <?php
function tentukan_nilai($number)
{
    if ($number >= 85 && $number <= 100) {
      echo "sangat baik <br>";
    }
    elseif ($number <  85 && $number >= 70 ) {
      echo "baik <br>";
    }
    elseif ($number < 70 && $number >= 60 ) {
      echo "cukup <br>";
    }
    else {
      echo "Kurang";
    }
}

//TEST CASES'
echo "<h1>Kalo 98</h1> <br>";
echo tentukan_nilai(98); //Sangat Baik
echo "<h1>Kalo 76</h1> <br>";
echo tentukan_nilai(76); //Baik
echo "<h1>Kalo 67</h1> <br>";
echo tentukan_nilai(67); //Cukup
echo "<h1>Kalo 43</h1> <br>";
echo tentukan_nilai(43); //Kurang
?>

  </body>
</html>
