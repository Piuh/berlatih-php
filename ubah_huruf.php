<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <?php
function ubah_huruf($string){
//kode di sini
$alphabet = "abcdefghijklmnopqrstuvwxyz";
$tampung = "";
for ($i=0; $i < strlen($string); $i++) {
  $position = strpos($alphabet,$string[$i]);
  $tampung .= substr($alphabet , $position +1,1);
}
return $tampung. "<br>";

}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>

  </body>
</html>
